from report_xls.report_xls import report_xls
from report import report_sxw
from datetime import date
from datetime import datetime
import time
import tools
import xlwt
from PIL import Image
from base64 import decodestring

class sale_account_invoice_parser_xls(report_sxw.rml_parse):
    
    def __init__(self, cr, uid, name, context):
        super(sale_account_invoice_parser_xls, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
                                  'get_object'      : self._get_object,
                                  'get_total'        : self._get_total,
                                  'get_tax_info'    : self._get_tax_info,
                                  })

    def _get_object(self,data):
        obj_data=self.pool.get(data['model']).browse(self.cr,self.uid,[data['id']])
        return obj_data

    def _get_total(self,invoice_lines,tax_lines):
        res = {
        'total_qty' : 0.0,
        'total_net' : 0.0,
        'total_tax' : 0.0,
        'total_amt' : 0.0,

        'weight'    : 0.0,
        'weight_net': 0.0
        }
        for line in invoice_lines:
            weight = line.product_id and line.product_id.weight or 0.0
            weight_net = line.product_id and line.product_id.weight_net or 0.0

            res['total_qty'] += line.quantity
            res['total_net'] += line.price_subtotal
            res['weight']    += weight
            res['weight_net']+= weight_net

        for line in tax_lines:
            res['total_tax'] += line.amount

        res['total_amt'] = res['total_net'] + res['total_tax']
        return res

    def _get_tax_info(self,invoice_lines):
        res={
        'code'      : '',
        'percentage': '',
        'name'      : ''
        }
        for line in invoice_lines:
            for tax in line.invoice_line_tax_id:
                res['code']         = tax.description or ''
                res['percentage']   = str(int((tax.amount * 100)))
                res['name']         = tax.name
        return res

class sale_account_invoice_xls(report_xls):
    def generate_xls_report(self, c, _xs, data, objects, wb):
        # print "1.",self
        # for key in c.keys():
        #     print "2",key,c[key]
        # print "3.",_xs
        # print "4.",data
        # print "5.",objects
        # print "6.",wb
        cr, uid = self.cr, self.uid
        inv_obj = self.pool.get('account.invoice')
        invoice = inv_obj.browse(cr,uid,c['active_id'])
        report_name = "Sale Invoice XLS"
        
        ws = wb.add_sheet("Invoice")
        
        #=======================================================================
        # STYLE
        #=======================================================================
        title           = xlwt.easyxf('font: height 350, name Arial, colour_index black, bold on; align: horiz centre;')
        strong          = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on; align: horiz left;')
        header_right    = xlwt.easyxf('''
                            font: height 200, name Arial, colour_index white, bold on; 
                            align: horiz centre, vert centre; 
                            borders: left thin, left_colour white, top thin, top_colour black, right thin, right_colour black, bottom thin, bottom_colour black; 
                            pattern: pattern solid, fore_color light_blue''')
        header_left     = xlwt.easyxf('''
                            font: height 200, name Arial, colour_index white, bold on; 
                            align: horiz centre, vert centre; 
                            borders: left thin, left_colour black, top thin, top_colour black, right thin, right_colour white, bottom thin, bottom_colour black; 
                            pattern: pattern solid, fore_color light_blue''')
        header_in       = xlwt.easyxf('''
                            font: height 200, name Arial, colour_index white, bold on; 
                            align: horiz centre, vert centre; 
                            borders: left thin, left_colour white, top thin, top_colour black, right thin, right_colour white, bottom thin, bottom_colour black; 
                            pattern: pattern solid, fore_color light_blue''')
        border_black    = xlwt.easyxf('''
                            borders: left thin, left_colour black, top thin, top_colour black, right thin, right_colour black, bottom thin, bottom_colour black;
         ''')
        border_grey     = xlwt.easyxf('''
                            borders: left thin, left_colour gray25, top thin, top_colour gray25, right thin, right_colour gray25, bottom thin, bottom_colour gray25;
         ''')
        border_grey_top = xlwt.easyxf('''
                            borders: left thin, left_colour gray25, top thin, top_colour gray25, right thin, right_colour gray25, bottom no_line, bottom_colour gray25;
         ''')
        border_grey_bot = xlwt.easyxf('''
                            font: bold on; 
                            borders: left thin, left_colour gray25, top no_line, top_colour gray25, right thin, right_colour gray25, bottom thin, bottom_colour gray25;
         ''')
        number          = xlwt.easyxf(num_format_str='#,##0;(#,##0)')
        number2d        = xlwt.easyxf(num_format_str='#,##0.00;(#,##0.00)')
        percent         = xlwt.easyxf(num_format_str='0%')

        #=======================================================================
        # COLUMN WIDTH
        #=======================================================================
        ws.col(0).width = 200
        for i in range(1,50):
            ws.col(i).width = 1400
        ws.row(16).height = 400
        
        #=======================================================================
        # HEADER
        #=======================================================================
        ws.row(2).height=600
        ws.write_merge(2,2,4,15,'SITI FAR EAST PTE. LTD.',title)


        #=======================================================================
        # UPPER SECTION
        #=======================================================================
        row=5
        ws.write_merge(row,row,1,3,'Document Type',border_grey_top)
        ws.write_merge(row,row,4,5,'No',border_grey_top)
        ws.write_merge(row,row,6,8,'Date',border_grey_top)
        ws.write_merge(row,row,9,10,'Page',border_grey_top)
        
        row=6
        ws.write_merge(row,row,1,3,'Sale Invoice',border_grey_bot)
        ws.write_merge(row,row,4,5,invoice.number or '',border_grey_bot)
        ws.write_merge(row,row,6,8,time.strftime('%d/%m/%y', time.strptime( invoice.date_invoice,'%Y-%m-%d')),border_grey_bot)
        ws.write_merge(row,row,9,10,'',border_grey_bot)
        
        row=7
        ws.write_merge(row,row,1,5,'Customer Number',border_grey_top)
        ws.write_merge(row,row,6,10,'GST Number',border_grey_top)
        
        row=8
        ws.write_merge(row,row,1,5,'',border_grey_bot)
        ws.write_merge(row,row,6,10,'',border_grey_bot)
        
        row=9
        ws.write_merge(row,row,1,10,'Payment Terms',border_grey_top)

        row=10
        ws.write_merge(row,row,1,10,'',border_grey_bot)

        row=11
        ws.write_merge(row,row,1,4,'Shipment Method',border_grey_top)
        ws.write_merge(row,row,5,7,'Transport',border_grey_top)
        ws.write_merge(row,row,8,10,'Transport',border_grey_top)
        
        row=12
        ws.write_merge(row,row,1,4,'',border_grey_bot)
        ws.write_merge(row,row,5,7,'',border_grey_bot)
        ws.write_merge(row,row,8,10,'',border_grey_bot)
        
        row=13
        ws.write_merge(row,row,1,6,'Bank',border_grey_top)
        ws.write_merge(row,row,7,8,'Swift Code',border_grey_top)
        ws.write_merge(row,row,9,10,'Currency',border_grey_top)
        
        row=14
        ws.write_merge(row,row,1,6,'',border_grey_bot)
        ws.write_merge(row,row,7,8,'',border_grey_bot)
        ws.write_merge(row,row,9,10,'',border_grey_bot)
        
        #=======================================================================
        # INVOICE ADDRESS
        #=======================================================================
        ws.write(5,13,'Dear Sir/Madam')
        ws.write(6,13,invoice.partner_id.name,strong)
        ws.write(7,13,invoice.partner_id.street)
        ws.write(8,13,invoice.partner_id.street2 or '-')
        ws.write(9,13,invoice.partner_id.city + invoice.partner_id.country.name + '-' + invoice.partner_id.zip)

        #=======================================================================
        # INVOICE ITEMS
        #=======================================================================
        row=16
        ws.write_merge(row,row,1,2,'Item',header_left)
        ws.write_merge(row,row,3,8,'Description',header_in)
        ws.write_merge(row,row,9,9,'Pos',header_in)
        ws.write_merge(row,row,10,10,'Um',header_in)
        ws.write_merge(row,row,11,11,'Qty',header_in)
        ws.write_merge(row,row,12,13,'Price',header_in)
        ws.write_merge(row,row,14,15,'Disc (%)',header_in)
        ws.write_merge(row,row,16,17,'Amount',header_in)
        ws.write_merge(row,row,18,18,'C.V.',header_right)
        
        row=17
        for line in invoice.invoice_line:
            ws.write_merge(row,row,1,2,line.product_id and line.product_id.default_code or '',border_black)
            ws.write_merge(row,row,3,8,line.name,border_black)
            ws.write_merge(row,row,9,9,'',border_black)
            ws.write_merge(row,row,10,10,line.uos_id and line.uos_id.name or '',border_black)
            ws.write_merge(row,row,11,11,line.quantity,border_black)
            ws.write_merge(row,row,12,13,line.price_unit,border_black)
            ws.write_merge(row,row,14,15,line.discount,border_black)
            ws.write_merge(row,row,16,17,line.price_subtotal,border_black)
            ws.write_merge(row,row,18,18,'',border_black)
            row += 1
        
        #=======================================================================
        # LOWER SECTION
        #=======================================================================
        row += 1
        ws.write_merge(row,row,1,4,'Total Goods',border_grey)
        ws.write_merge(row,row,5,9,'Net Amount',border_grey)
        ws.write_merge(row,row,10,13,'Total GST',border_grey)
        ws.write_merge(row,row,14,18,'Total Amount',border_grey)

        row += 1
        ws.row(row).height = 500
        ws.write_merge(row,row,1,4,c.get_total(invoice.invoice_line,invoice.tax_line)['total_qty'],border_grey)
        ws.write_merge(row,row,5,9,c.get_total(invoice.invoice_line,invoice.tax_line)['total_net'],border_grey)
        ws.write_merge(row,row,10,13,c.get_total(invoice.invoice_line,invoice.tax_line)['total_tax'],border_grey)
        ws.write_merge(row,row,14,18,c.get_total(invoice.invoice_line,invoice.tax_line)['total_amt'],border_grey)

        row += 1
        ws.write_merge(row,row,1,3,'Tax Code',border_grey)
        ws.write_merge(row,row,4,6,'Taxable',border_grey)
        ws.write_merge(row,row,7,9,'%GST',border_grey)
        ws.write_merge(row,row,10,13,'Tax',border_grey)
        ws.write_merge(row,row,14,18,'Total Invoice',border_grey)

        row += 1
        ws.row(row).height = 500
        ws.write_merge(row,row,1,3,c.get_tax_info(invoice.invoice_line)['code'],border_grey)
        ws.write_merge(row,row,4,6,c.get_total(invoice.invoice_line,invoice.tax_line)['total_net'],border_grey)
        ws.write_merge(row,row,7,9,c.get_tax_info(invoice.invoice_line)['percentage'],border_grey)
        ws.write_merge(row,row,10,13,c.get_tax_info(invoice.invoice_line)['name'],border_grey)
        ws.write_merge(row,row,14,18,c.get_total(invoice.invoice_line,invoice.tax_line)['total_amt'],border_grey)

        row += 1
        ws.row(row).height = 500
        ws.write_merge(row,row,1,12,'',border_grey)
        ws.write_merge(row,row,13,18,'',border_grey)

        row += 1
        ws.write_merge(row,row,1,9,'Packaging Exterior Aspect',border_grey)
        ws.write_merge(row,row,10,12,'Packages',border_grey)
        ws.write_merge(row,row,13,15,'Net Weight (kg)',border_grey)
        ws.write_merge(row,row,16,18,'Gross Weight (kg)',border_grey)

        row += 1
        ws.row(row).height = 500
        ws.write_merge(row,row,1,9,'',border_grey)
        ws.write_merge(row,row,10,12,'',border_grey)
        ws.write_merge(row,row,13,15,c.get_total(invoice.invoice_line,invoice.tax_line)['weight_net'],border_grey)
        ws.write_merge(row,row,16,18,c.get_total(invoice.invoice_line,invoice.tax_line)['weight'],border_grey)
        pass

sale_account_invoice_xls('report.sale.account.invoice.xls','account.invoice',parser=sale_account_invoice_parser_xls)
