<html>
	<head>
		<meta content="text/html; charset=UTF-8" http-equiv="content-type"/>
		<script type="text/javascript">
			function subst() {
			var vars={};
			var x=document.location.search.substring(1).split('&');
			for(var i in x) {var z=x[i].split('=',2);vars[z[0]] = unescape(z[1]);}
			var x=['frompage','topage','page','webpage','section','subsection','subsubsection'];
			for(var i in x) {
			var y = document.getElementsByClassName(x[i]);
			for(var j=0; j<y.length; ++j) y[j].textContent = vars[x[i]];
				}
			}


			var pdfInfo = {};
			var x = document.location.search.substring(1).split('&');
			for (var i in x) { var z = x[i].split('=',2); pdfInfo[z[0]] = unescape(z[1]); }
			function getPdfInfo() {
			var page = pdfInfo.page || 1;
			var pageCount = pdfInfo.topage || 1;
			document.getElementById('pdfkit_page_current').textContent = page;
			document.getElementById('pdfkit_page_count').textContent = pageCount;
			}			
		</script>

		<style>
			.break { page-break-after: always; }
			
			body
			{
				font-size: 12px;
				min-height: 100%;
			}

			h2
			{
			text-align:'center';
			}
			
			table.no_border
			{
			border-collapse:collapse;
			padding:10px;
			width:100%;
			}
			
			table.general
			{
			border-collapse:collapse;
			border: 1px solid LightGrey;
			padding:10px;
			width:100%;
			}

			td.general
			{
			border: 1px solid LightGrey;
			}

			table.content
			{
			border-collapse:collapse;
			border: 1px solid black;
			}

			td.content
			{
			border: 1px solid black;
			}

			th.content
			{
			border: 1px solid black;
			background-color: #0077b3;
			color: white;
			}

			.footer 
			{
				position: absolute;
				right: 0;
				bottom: 0;
				left: 0;
				padding: 1rem;
				background-color: #efefef;
				text-align: center;
			}
			td.data
			{
			border-bottom:thin solid black;
			width:69%;
			}
			
			.var
			{
			font-weight:bold;
			}
			
			.left
			{
				text-align: left;
			}

			.center
			{
				text-align: center;
			}

			.right
			{
				text-align: right;
			}

			.padded
			{
			text-align:center;
			padding-top:30px;
			}
			
			.ttd
			{
			text-align:center;
			padding-bottom:80px;
			width:50%
			}
			
			.nama
			{
			text-align:center;
			width:50%;
			font-weight:bold;
			}
			
			
		</style>
	</head>
	<body onload="getPdfInfo()">
		%for o in objects:
		<table width="100%">
			<tr>
				<td width="65%">
					<table class="general" cellpadding="5px">
						<tr>
							<td width="33%" class="center general">
								Document Type <br />
								<b>Commercial Invoice</b>
							</td>
							<td width="23%" class="center general" >
								No <br />
								<b>${o.number or '-'}</b>
							</td>
							<td class="center general">
								Date <br />
								<b>${o.date_invoice and time.strftime('%d/%m/%y', time.strptime( o.date_invoice,'%Y-%m-%d')) or ' - '}</b>
							</td>
							<td class="center general">
								Page <br />
								<span id="pdfkit_page_current"></span>
								<!-- / <span id="pdfkit_page_count"></span> -->
							</td>
						</tr>
						<tr>
							<td class="general" colspan="2">
								Customer Number <br />
								<b>${o.partner_id.customer_number or ''} ${o.partner_id.name or ''}</b>
							</td>
							<td class="general" colspan="2">
								GST Number <br />
								<b>${o.company_id.vat or '-'}</b>
							</td>
						</tr>
						<tr>
							<td class="general" colspan="4">
								Payment Terms <br />
								<b>${o.payment_term and o.payment_term.note or '-'}</b>
							</td>
						</tr>
						<tr>
							<% order = get_order(o) %>
							<td class="general" colspan="2">
								Shipment Method <br />
								%if o.shipment_method:
								<b>${o.shipment_method}</b>
								%else:
								%if order:
								<b>${order.shipment_method or ''}</b>
								%else:
								<b>&nbsp;</b>
								%endif
								%endif
							</td>
							<td colspan="2" class="general">
								Transport <br />
								%if o.incoterm:
								<b>${o.incoterm.name}</b>
								%else:
								%if order:
								<b>${get_order(o).incoterm and get_order(o).incoterm.name or '-'}</b>
								%else:
								<b>&nbsp;</b>
								%endif
								%endif
							</td>
						</tr>
						<tr>
							<td class="general" colspan="2">
								Bank <br />
								%if o.partner_bank_id:
								<b>${o.partner_bank_id and o.partner_bank_id.bank_name or ' ' }</b> <br />
								ACCOUNT: <b>${o.partner_bank_id and o.partner_bank_id.acc_number or ' '}</b>
								%else:
								<b>&nbsp;</b>
								%endif
							</td>
							<td class="general" style="vertical-align: top;">
								Swift Code <br />
								<b>${o.partner_bank_id.bank_bic or '-'}</b>
							</td>
							<td class="center general" style="vertical-align: top;">
								Currency <br />
								<b>${o.currency_id.name}</b>
							</td>
						</tr>
					</table>
				</td>
				<td width="35%" style="padding-left: 30px; vertical-align: top;">
					Dear Sir/Madam, <br/>
					%if o.partner_id.is_company:
					<b>${o.partner_id.name}</b><br/>
					%else:
					<b>${o.partner_id.name}</b><br/>
					<b>${o.partner_id.parent_id and o.partner_id.parent_id.name or ''}</b><br/>
					%endif
					${o.partner_id.street}<br/>
					${o.partner_id.street2 or ''} <br/>
					${o.partner_id.city or ''} ${o.partner_id.zip and ' - '+o.partner_id.zip or ''}<br/>
					${o.partner_id.country_id.name}

				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
		<table width="100%" class="content" cellpadding="5px">
			<tr>
				<th class="content" width="20%">Item</th>
				<th class="content" width="35%">Description</th>
				<th class="content" width="5%">Pos</th>
				<th class="content" width="5%">Um</th>
				<th class="content" width="5%">Qty</th>
				<th class="content" width="10%">Price</th>
				<th class="content" width="5%">Disc(%)</th>
				<th class="content" width="10%">Amount</th>
				<th class="content" width="5%">C.V.</th>
			</tr>
			%for line in o.invoice_line:
			%if line.price_unit == 0.0:
			<tr>
				<td colspan="9" style="padding-left: 130px">
					${line.name}
				</td>
			</tr>
			%else:
			<tr>
				<td class="content">${line.product_id and line.product_id.default_code or ''}</td>
				<td class="content">${line.product_id and line.product_id.name or ''}</td>
				<td class="content">${get_order_line(line)}</td>
				<td class="content">${line.uos_id and line.uos_id.name or ''}</td>
				<td class="content right">${formatLang(line.quantity,digits=0)}</td>
				<td class="content right">${formatLang(line.price_unit,digits=2)}</td>
				<td class="content right">${formatLang(line.discount,digits=2)}</td>
				<td class="content right">${formatLang(line.price_subtotal,digits=2)}</td>
				<td class="content">${get_tax(line)}</td>
			</tr>
			%endif
			%endfor
		</table>
		${o.comment or ''}
		<br /><br />
		<table class="general" width="100%" cellpadding="1px" style="page-break-inside: avoid;">
			<tr>
				<td class="general" colspan="3" width="25%">Total Goods</td>
				<td class="general" colspan="3" width="25%">Net Amount</td>
				<td class="general" colspan="3" width="25%">Total GST</td>
				<td class="general" colspan="3" width="25%">Total Amount</td>
			</tr>
			<tr>
				<td class="general" colspan="3" style="padding: 5px; vertical-align: middle; height: 25px;">${formatLang(get_total(o.invoice_line,o.tax_line)['total_net'],digits=2)}</td>
				<td class="general" colspan="3" style="padding: 5px; vertical-align: middle; height: 25px;">${formatLang(get_total(o.invoice_line,o.tax_line)['total_net'],digits=2)}</td>
				<td class="general" colspan="3" style="padding: 5px; vertical-align: middle; height: 25px;">${formatLang(get_total(o.invoice_line,o.tax_line)['total_tax'],digits=2)}</td>
				<td class="general" colspan="3" style="padding: 5px; vertical-align: middle; height: 25px;">${formatLang(get_total(o.invoice_line,o.tax_line)['total_amt'],digits=2)}</td>
			</tr>
			<tr>
				<td class="general" colspan="2">Tax Code</td>
				<td class="general" colspan="2">Taxable</td>
				<td class="general" colspan="2">% GST</td>
				<td class="general" colspan="3">Tax</td>
				<td class="general" colspan="3">Total Invoice</td>
			</tr>
			<tr>
				<td class="general" colspan="2" style="padding: 5px; vertical-align: middle; height: 25px;">${get_tax_info(o.invoice_line)['code']}</td>
				<td class="general" colspan="2" style="padding: 5px; vertical-align: middle; height: 25px;">${formatLang(get_total(o.invoice_line,o.tax_line)['total_net'],digits=2)}</td>
				<td class="general" colspan="2" style="padding: 5px; vertical-align: middle; height: 25px;">${get_tax_info(o.invoice_line)['percentage']}</td>
				<td class="general" colspan="3" style="padding: 5px; vertical-align: middle; height: 25px;">${get_tax_info(o.invoice_line)['name']}</td>
				<td class="general" colspan="3" style="padding: 5px; vertical-align: middle; height: 25px;">${formatLang(o.amount_total,digits=2)}</td>
			</tr>
			<tr>
				<td class="general" colspan="8" style="padding: 5px; vertical-align: middle; height: 25px;">&nbsp;</td>
				<td class="general" colspan="4" style="padding: 5px; vertical-align: middle; height: 25px;">&nbsp;</td>
			</tr>
			<tr>
				<td class="general" colspan="6">Packing exterior aspect</td>
				<td class="general" colspan="2">Package</td>
				<td class="general" colspan="2">Net Weight (kg)</td>
				<td class="general" colspan="2">Gross Weight (kg)</td>
			</tr>
			<tr>
				<td class="general" colspan="6" style="padding: 5px; vertical-align: middle; height: 25px;">${o.exterior_aspect or '-'}</td>
				<td class="general" colspan="2" style="padding: 5px; vertical-align: middle; height: 25px;">${o.package or '-'}</td>
				<td class="general" colspan="2" style="padding: 5px; vertical-align: middle; height: 25px;">${o.net_weight or '0.0'}</td>
				<td class="general" colspan="2" style="padding: 5px; vertical-align: middle; height: 25px;">${o.gross_weight or '0.0'}</td>
			</tr>
		</table>
		%endfor
	</body>
</html>