from report import report_sxw
from datetime import date
from datetime import datetime
import time
import tools
import unicodedata

class sale_quotation(report_sxw.rml_parse):
	
	def __init__(self, cr, uid, name, context):
		super(sale_quotation, self).__init__(cr, uid, name, context=context)
		self.localcontext.update({
								  'get_object'      : self._get_object,
								  'get_bank'		: self._get_bank,
								  'get_total'		: self._get_amount
								  })

	def _get_object(self,data):
		obj_data=self.pool.get(data['model']).browse(self.cr,self.uid,[data['id']])
		return obj_data

	def _get_bank(self,partner):
		if partner.bank_ids:
			bank_obj=partner.bank_ids[0]
			bank={
			'name': bank_obj.bank_name,
			'acc_number': bank_obj.acc_number,
			'bank_bic': bank_obj.bank_bic
			}
		else:
			bank={
			'name': ' ',
			'acc_number': ' ',
			'bank_bic': ' '
			}
		return bank

	def _get_amount(self,order):
		amount = {
		'gross':0.0,
		'discount':0.0,
		'net':0.0,
		'total':0.0
		}
		for line in order.order_line:
			line_gross = line.product_uom_qty * line.price_unit
			line_discount = line.product_uom_qty * line.price_unit * line.discount / 100.0
			
			amount['gross'] += line_gross
			amount['discount'] += line_discount

		amount['net'] = order.amount_untaxed
		amount['total'] = order.amount_total
		return amount




report_sxw.report_sxw('report.sale.quotation', 'sale.order', 'sale_invoice_template/report/report_sale_quotation.mako', parser=sale_quotation)