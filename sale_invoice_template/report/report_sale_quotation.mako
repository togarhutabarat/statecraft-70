<html>
<head>
	<title>Quotation</title>
		<style>
			.break { page-break-after: always; }
			
			body
			{
				font-size: 12px;
				min-height: 100%;
			}

			h2
			{
			text-align:'center';
			}
			
			table.no_border
			{
			border-collapse:collapse;
			padding:10px;
			width:100%;
			}
			
			table.general
			{
			border-collapse:collapse;
			border: 1px solid LightGrey;
			padding:10px;
			width:100%;
			}

			td.general
			{
			border: 1px solid LightGrey;
			}

			td.border_top_bottom
			{
				border-top: 2px solid black;
				border-bottom: 2px solid black;
				text-align: center;
				border-collapse: collapse;
			}

			table.content
			{
			border-collapse:collapse;
			border: 1px solid black;
			}

			td.content
			{
			border: 1px solid black;
			}

			th.content
			{
			border: 1px solid black;
			background-color: #0077b3;
			color: white;
			}

			.footer 
			{
				position: absolute;
				right: 0;
				bottom: 0;
				left: 0;
				padding: 1rem;
				background-color: #efefef;
				text-align: center;
			}
			td.data
			{
			border-bottom:thin solid black;
			width:69%;
			}
			
			.var
			{
			font-weight:bold;
			}
			
			.left
			{
				text-align: left;
			}

			.center
			{
				text-align: center;
			}

			.right
			{
				text-align: right;
			}

			.padded
			{
			text-align:center;
			padding-top:30px;
			}
			
			.ttd
			{
			text-align:center;
			padding-bottom:80px;
			width:50%
			}
			
			.nama
			{
			text-align:center;
			width:50%;
			font-weight:bold;
			}
			
			
		</style>
</head>
<body>
	%for o in objects:
	<h2 style="text-align: center;">QUOTATION</h2>
	<table width="100%" cellpadding="1px" cellspacing="0">
		<tr>
			<td colspan="4" width="50%">&nbsp;</td>
			<td colspan="2" width="30%">
				Number <br/>
				<b>${o.name}</b>
			</td>
			<td width="10%">
				Date <br />
				<b>${time.strftime('%d/%m/%y', time.strptime( o.date_order,'%Y-%m-%d')) |entity}</b>
			</td>
			<td width="10%">
				Page <br />
				<b>1</b>
			</td>
		</tr>
		<tr>
			<td> </td>
		</tr>
		<tr>
			<td colspan="4">
				<b>Ship To</b> <br />
				<b>${o.partner_shipping_id.name}</b> <br />
				${o.partner_shipping_id.street} <br />
				%if o.partner_shipping_id.street2:
				${o.partner_shipping_id.street2} <br /> 
				%endif
				${o.partner_shipping_id.zip or ''} ${o.partner_shipping_id.city or ''} <br />
				${o.partner_shipping_id.country_id and o.partner_shipping_id.country_id.name or ''} <br />
			</td>
			<td colspan="4">
				<b>Dear Sir/Madam</b> <br />
				<b>${o.partner_id.name}</b> <br />
				${o.partner_id.street} <br />
				%if o.partner_id.street2: 
				${o.partner_id.street2} <br /> 
				%endif
				${o.partner_id.zip or ''} ${o.partner_id.city or ''} <br />
				${o.partner_id.country_id and o.partner_id.country_id.name or ''} <br />
			</td>
		</tr>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<b>Customer</b> <br />
				${o.partner_id.customer_number or ' '} ${o.partner_id.name or ' '} 
			</td>
			<td colspan="2">
				<b>Tax Number</b> <br />
				${o.partner_id.country_id and o.partner_id.country_id.code or '-'}
			</td>
			<td colspan="2">
				<b>Currency</b> <br />
				${o.currency_id.name}
			</td>
		</tr>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<b>Shipping</b> <br />
				<b>${o.shipment_method or ''}</b>
			</td>
			<td colspan="2">
				<b>Terms of Delivery</b> <br />
				${o.incoterm and o.incoterm.name or '-'}
			</td>
			<td colspan="2">
				<b>Validity Date</b> <br />
				${time.strftime('%d/%m/%y', time.strptime( o.validity_date,'%Y-%m-%d')) |entity}
			</td>
		</tr>
		<tr>
			<td colspan="8">
				<hr />
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<b>Customer Reference</b> <br />
				${o.client_order_ref or ' '}
			</td>
			<td colspan="4">
				<b>Payment</b> <br />
				${o.payment_term.note or ' '}
			</td>
		</tr>
		<tr>
			<td colspan="8">
				<b>Bank</b> <br />
				${get_bank(o.company_id.partner_id)['name']} <br />
				ACCOUNT ${get_bank(o.company_id.partner_id)['acc_number']} - SWIFT CODE ${get_bank(o.company_id.partner_id)['bank_bic']}
			</td>
		</tr>
	</table>
	<p />

	<table class="content" width="100%" cellpadding="5px" cellspacing="0px">
		<tr>
			<th class="content" width="5%">Pos/Seq</th>
			<th class="content" width="40%">Item/Description</th>
			<th class="content" width="10%">Price</th>
			<th class="content" width="10%">UoM</th>
			<th class="content" width="5%">Qty</th>
			<th class="content" width="10%">Discount</th>
			<th class="content" width="10%">Amount</th>
			<th class="content" width="10%">Time of Delivery</th>
		</tr>
		%for line in o.order_line:
		%if line.price_unit == 0.0:
		<tr>
			<td colspan="8" style="padding-left: 60px">
				${line.name}
			</td>
		</tr>
		%else:
		<tr>
			<td class="content">${line.pos or ' '}</td>
			%if line.product_id:
			<td class="content">[${line.product_id.default_code}] ${line.product_id.name}</td>
			%else:
			<td class="content">${line.name}</td>
			%endif
			<td class="content">${line.price_unit}</td>
			<td class="content">${line.product_uom.name}</td>
			<td class="content">${line.product_uom_qty}</td>
			<td class="content">${line.discount}</td>
			<td class="content">${line.price_subtotal}</td>
			<td class="content">${int(line.delay/7)} week(s)</td>
		</tr>
		%endif
		%endfor
	</table>
	<p /><p />
	<table width="100%" cellspacing="0px" cellpadding="2px">
		<tr>
			<td width="25%" class="border_top_bottom">Gross Amount</td>
			<td width="25%" class="border_top_bottom">Total Discount</td>
			<td width="25%" class="border_top_bottom">Net Amount</td>
			<td width="25%" class="border_top_bottom">Total</td>
		</tr>
		<tr>
			<td align="center">${formatLang(get_total(o)['gross'])}</td>
			<td align="center">${formatLang(get_total(o)['discount'])}</td>
			<td align="center">${formatLang(get_total(o)['net'])}</td>
			<td align="center">${formatLang(get_total(o)['total'])}</td>
		</tr>
	</table>
	%endfor
</body>
</html>