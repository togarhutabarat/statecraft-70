from osv import osv,fields

class stock_picking_out(osv.osv):
    _name = "stock.picking.out"
    _inherit = "stock.picking"
    _table = "stock_picking"
    _columns = {
    	'exterior_aspect': fields.char('Packing Exterior Aspect',size=200),
    	'package': fields.integer('Package'),
		'gross_weight': fields.float('Gross Weight (kg)'),
		'net_weight': fields.float('Net Weight (kg)')
    }
