{
    "name" : "SFE Sale Invoice",
    "version" : "1.0",
    "depends" : ["sale","account","report_webkit","report_xls","stock"],
    "author" : "Togar Hutabarat",
    "description": """This module will generate a custom sale invoice template for Siti Far East Pte. Ltd.
    """,
    "website" : "",
    "category" : "Sales",
    "init_xml" : [],
    "demo_xml" : [],
    'test': [],
    "update_xml" : [
        "data.xml",
        "partner_view.xml",
        "sale_view.xml",
        "stock_view.xml",
        "account_view.xml",
        "report/report_sale_invoice.xml",
        "report/report_sale_quotation.xml",
        "report/report_delivery_order.xml",
        # "report/report_sale_invoice_xls.xml"
    ],
    "active": False,
    "installable": True,
}